/*
 Eric Wallace
 Dec. 19, 2013
 Ch. 3 Exercises
 Michael Dawson : Beginning C++ Through Game Programming, Third Edition
 */

#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <array>

void jumble();
void clearscreen();
void mainMenu();
void exercise2();
void exercise3();

int main()
{
    
    mainMenu();
}

void mainMenu()
{
    int exercise;
    
    while (exercise != 4)
    {
        clearscreen();
        std:: cout << "Chapter 3 Exercises\n\n";
        
        std:: cout << "1 - Word Jumble Game\n";
        std:: cout << "2 - Whats wrong with the code?\n";
        std:: cout << "3 - Whats wrong with the code?\n";
        std:: cout << "4 - Exit Program\n\n";
        
        std:: cout << "Which exercise do you want to play ?" << std:: endl;
        std:: cin >> exercise;
        
        switch (exercise)
        {
            case 1:
                clearscreen();
                jumble();
                break;
            case 2:
                clearscreen();
                exercise2();
                std:: cin.ignore();
                std:: cin.get();
                break;
            case 3:
                clearscreen();
                exercise3();
                std:: cin.ignore();
                std:: cin.get();
                break;
            case 4:
                clearscreen();
                std:: cout << "Closing Program. . ." << std:: endl;
                return;
                break;
            default:
                clearscreen();
                std:: cout << "Did not enter valid exercise number" << std:: endl;
                std:: cin.ignore();
                std:: cin.get();
                break;
                
        }
 
    }
}

void jumble()
{
    bool playing = true;
    int score = 0;
    
    while(playing)
    {
        enum fields {WORD, HINT, NUM_FIELDS};
        const int NUM_OF_WORDS = 5;
        const int POINTS[NUM_OF_WORDS] = {25, 75, 50, 100, 15};
        const std:: string WORDS[NUM_OF_WORDS][NUM_FIELDS] =
        {
            {"doobies", "Rolling blunts, Rolling _ _ _ _ _ _ _ up, Smoking Sections",},
            {"breezies", "Bitches that be too easy."},
            {"clothes", "So I just change _ _ _ _ _ _ _, and go . . ."},
            {"nigga", "Im a real . . ."},
            {"weather", "Women, Weed, and _ _ _ _ _ _ _ ."}
        };
    
        std:: string guess, hint, theWord, theHint;
        int index1, index2, choice, pointValue;
    
        // Randomly chooses each word for play
        srand(static_cast<unsigned int>(time(0)));
        choice = rand() % NUM_OF_WORDS;
        theWord = WORDS[choice][WORD];
        theHint = WORDS[choice][HINT];
        pointValue = POINTS[choice];
    
        // Jumbles up the word chosesn
        std:: string jumble = theWord;
        int length = jumble.size();
        for(int i = 0; i < length; ++i)
        {
            index1 = rand() % length;
            index2 = rand() % length;
            char temp = jumble[index1];
            jumble[index1] = jumble[index2];
            jumble[index2] = temp;
        }
    
    
        while ((guess != theWord) && (guess != "quit"))
        {
            clearscreen();
            std:: cout << "\tWord Jumble v1.0" << std:: endl;
            std:: cout << "The classic guess the word game." << std:: endl << std:: endl;
            
            std:: cout << "Unscramble the letters to make a word." << std:: endl;
            std:: cout << "Enter 'hint' for a hint.(deducts points)" << std:: endl;
            std:: cout << "Enter 'quit' to quit the game." << std:: endl << std:: endl;
            
            std:: cout << "The jumble is: " << jumble << std:: endl << std:: endl;
            
            std:: cout << "Your Score: " << score << std:: endl;
            std:: cout << "Your guess: ";
            std:: cin >> guess;
            
            if(guess == "hint")
            {
                // Deducts half points for asking for hint
                pointValue = (pointValue - (pointValue / 2));
                std:: cout << theHint;
                std:: cin.ignore();
                std:: cin.get();
            }
            else if(guess == "quit")
            {
                std:: cout << "Thank you for playing." << std:: endl;
                std:: cin.ignore();
                std:: cin.get();
                return;
            }
            else if(guess != theWord)
            {
                // Deducts 1 point per miss
                pointValue = pointValue - 1;
                std:: cout << "NOPE, TRY AGAIN" << std:: endl;
                std:: cin.ignore();
                std:: cin.get();
            }
            else
            {
                score = score + pointValue;
                std:: cout << "YOU GOT IT !" << std:: endl;
                std:: cin.ignore();
                std:: cin.get();
            }

        }
    
        // Play Again Prompt
        char playAgain;
        std:: cout << "\nGood Job, Would you like to play again (y/n)?";
        std:: cin >> playAgain;
        playAgain = toupper(playAgain);
        if (playAgain == 'Y')
        {
            playing = true;
        }
        else
        {
            playing = false;
        }
    
    }
}

void clearscreen()
{
    for(int i = 0; i < 50; i++)
    {
        std:: cout << "\n" << std:: endl;
    }
}

void exercise2()
{
    std:: cout << "What's wrong with the following code?" << std:: endl;
    
    std:: cout << "\nfor (int i = 0; i <= phrase.size(); ++i)" << std:: endl;
    std:: cout << "{" << std:: endl;
    std:: cout << "     cout << \"Character at position \" << i << \" is: \" <<";
    std:: cout << " phrase[i] << endl;" << std:: endl;
    std:: cout << "}" << std:: endl;
    
    std:: cout << "\nIt will skip over the first position 0 , because '++i' ";
    std:: cout << "causes variable i to increase before code is executed.\n\n";
    
}

void exercise3()
{
    std:: cout << "What's wrong with the following code?" << std:: endl;
    
    std:: cout << "\nconst int ROWS = 2;\n";
    std:: cout << "const int COLUMNS = 3;\n";
    std:: cout << "char board[COLUMNS][ROWS] = { {'0', 'X', 'O'},\n";
    std:: cout << "                              {' ', 'X', 'X'} };\n";
    
    std:: cout << "\nThe columns and rows got switched. Rows always come first\n";
    std:: cout << "and columns always come second." << std:: endl;
}
