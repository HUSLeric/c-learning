/*
Eric Wallace
Chapter 2 Tutorial / Examples
HUSL Vision
copyrighted 2013
*/

#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;


int getGuess(int &s, int &high, int &low);

int main()
{
    int exercise;
    
    do {
        cout << "**********CHAPTER TWO**********" << endl << endl;
        
        cout << "1 - Menu Chooser Program" << endl;
        cout << "2 - Whats wrong with the loop?" << endl;
        cout << "3 - Guess My Number" << endl << endl;
        
        cout << "Which Exercise do you want to view ?" << endl;
        cin >> exercise;
        
        switch (exercise)
        {
                // Exercise 1
            case 1:
                enum difficulty {EASY, NORMAL, HARD};
                int choice;
                
                cout << "Difficulty Levels" << endl << endl;
                
                cout << "1 - Easy" << endl;
                cout << "2 - Normal" << endl;
                cout << "3 - Hard" << endl << endl;
                
                cout << "Choice: ";
                cin >> choice;
                choice = choice - 1;
                
                switch (choice)
            {
                case EASY:
                    cout << "You chose Easy." << endl;
                    break;
                case NORMAL:
                    cout << "You chose Normal." << endl;
                    break;
                case HARD:
                    cout << "You chose Hard." << endl;
                    break;
                default:
                    cout << "You did not choose a valid difficulty" << endl;
            }
                
                cin.ignore();
                cin.get();
               
                break;
                
                // Exercise 2
            case 2:
                cout << "Whats wrong with the following loop ?" << endl << endl;
                
                cout << "int x = 0;" << endl;
                cout << "while(x)" << endl;
                cout << "{" << endl;
                cout << "     ++x;" << endl;
                cout << "     cout << x << endl;" << endl;
                cout << "}" << endl << endl;
                
                cout << "The loop would never execute because x = 0, and by doing that ";
                cout << "when the while loop tests its expression it reads x, which is ";
                cout << "0, which is equivilent to false. So the code is never executed ";
                cout << "to ouput the value of x." << endl;
                
                cin.ignore();
                cin.get();
                
                break;
                
                // Exercise 3
            case 3:
                enum statuses {NEUTRAL, HIGH, LOW};
                int status = NEUTRAL;
                int cpuguess, high = 100, low = 1;
                char winner, highorlow, playAgain;
                
                do {
                    
                    cout << "Guess My Number" << endl << endl;
                    
                    cout << "Press Enter to Play !" << endl;
                    cin.ignore();
                    cin.get();
                    
                    do {
                        cpuguess = getGuess(status, high, low);
                        
                        cout << "Is your guess " << cpuguess << " ?" << endl;
                        cout << "(y/n)";
                        cin >> winner;
                        winner = toupper(winner);
                        
                        if(winner == 'N')
                        {
                        WrongInputHL:
                            cout << "Too HIGH or Too LOW ? O_O" << endl;
                            cout << "(h/l)";
                            cin >> highorlow;
                            highorlow = toupper(highorlow);
                            if(highorlow == 'H')
                            {
                                status = HIGH;
                                high = cpuguess;
                            }
                            else if(highorlow == 'L')
                            {
                                status = LOW;
                                low = cpuguess;
                            }
                            else
                            {
                                goto WrongInputHL;
                            }
                        }
                    } while (winner == 'N');
                    
                    cout << "I won ! :D" << endl << endl;
                    
                    cout << "Do you want to play again ?" << endl;
                    cout << "(y/n)" << endl;
                    cin >> playAgain;
                    playAgain = toupper(playAgain);
                }while(playAgain == 'Y');
                break;
        }

    }while (exercise != 4);
}

int getGuess(int &s, int &high, int &low)
{
    srand(static_cast<unsigned int>(time(0)));
    int guess;
    
    // Too High
    if(s == 1)
    {
    PickHAgain:
        guess = rand() % high + 1;
        if(guess < low)
        {
            goto PickHAgain;
        }
        return guess;
    }
    // Too Low
    else if(s == 2)
    {
    PickLAgain:
        guess = low + (1 + rand() % (100 - high));
        if(guess > high)
        {
            goto PickLAgain;
        }
        return guess;
    }
    // Neutral
    else
    {
            guess = rand() % 100 + 1;
            return guess;
    }
    
    
}

