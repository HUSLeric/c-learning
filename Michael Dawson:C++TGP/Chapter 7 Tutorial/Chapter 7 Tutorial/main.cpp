//
//  main.cpp
//  Chapter 7 Tutorial
//
//  Created by H U $ L on 12/24/13.
//  Copyright (c) 2013 Eric Wallace. All rights reserved.
//

#include <iostream>
#include <string>


void clearscreen(); // Clears Console Screen
void p2p(); // Pointer 2 Pointer program
void exercise3(); // Explain the code program
void exitMessage(); // Exit message
void errorScreen(); // Displays error message
void madLib(); // Mad Lib program
int askNumber(std:: string prompt);
std:: string askText(std:: string prompt);

void tellStory(const std:: string *const name, const std:: string *const noun,
               const std:: string *const bodyPart, const std:: string *const verb,
               const int *const number);

int main()
{
    bool playing = true;
    
    while(playing)
    {
        clearscreen();
        int exercise;
        std:: cout << "Chapter 6 Exercises\n\n";
        
        std:: cout << "1 - Pointer to a Pointer\n";
        std:: cout << "2 - MadLib v3.0\n";
        std:: cout << "3 - Whats going on in the code?\n";
        std:: cout << "4 - Exit program\n\n";
        
        std:: cout << "Which exercise do you want to view:";
        std:: cin >> exercise;
        
        switch(exercise)
        {
            case 1:
                clearscreen();
                p2p();
                break;
            case 2:
                clearscreen();
                madLib();
                std:: cin.ignore();
                std:: cin.get();
                break;
            case 3:
                clearscreen();
                exercise3();
                std:: cin.ignore();
                std:: cin.get();
                
                break;
            case 4:
                clearscreen();
                exitMessage();
                playing = false;
                break;
            default:
                clearscreen();
                errorScreen();
                std:: cin.ignore();
                std:: cin.get();
                
                break;
                
        }
        
    }

}

void clearscreen()
{
    for(int i = 0; i < 50; ++i)
    {
        std:: cout << "\n\n";
    }
}

void errorScreen()
{
    std:: cout << "Wrong input !" << std:: endl;
    
}

void exitMessage()
{
    std:: cout << "Thank you for using my program ! Good bye !" << std:: endl;
}

void p2p()
{
    std:: string str = "Eric";
    std:: string *pStr = &str;
    std:: string *pAPointer = pStr;
    
    std:: cout << "The size of str: " << (*pAPointer).size() << std:: endl;
    
    std:: cin.ignore();
    std:: cin.get();
    
    
}

void exercise3()
{
    std:: cout << "Will the three memory addresses displayed by the following\n";
    std:: cout << "program all be the same? Explain what's going on in the\n";
    std:: cout << "code.\n\n";
    
    std:: cout << "#include <iostream>\n";
    std:: cout << "using namespace std;\n\n";
    
    std:: cout << "int main()\n";
    std:: cout << "{\n";
    std:: cout << "     int a = 10;\n";
    std:: cout << "     int& b = a;\n";
    std:: cout << "     int* c = &b;\n\n";
    
    std:: cout << "     cout << &a << endl;\n";
    std:: cout << "     cout << &b << endl;\n";
    std:: cout << "     cout << &(*c) << endl;\n\n";
    
    std:: cout << "     return 0;\n";
    std:: cout << "}\n\n";
    
    std:: cout << "Yes. All three would display the same address.\n";
    std:: cout << "First int a is initialized and has an address (x).\n";
    std:: cout << "Then b is referenced to the address of a which is (x).\n";
    std:: cout << "Then c is a pointer initialized to the address of b,\n";
    std:: cout << "which is (x).\n\n";
    
    std:: cout << "This is what the code above displays. . . \n\n";
    
    int a = 10;
    int& b = a;
    int* c = &b;
    
    std:: cout << &a << std:: endl;
    std:: cout << &b << std:: endl;
    std:: cout << &(*c) << std:: endl;
    
}

std:: string askText(std:: string prompt)
{
    std:: string text;
    std:: cout << prompt;
    std:: cin >> text;
    return text;
}

int askNumber(std:: string prompt)
{
    int num;
    std:: cout << prompt;
    std:: cin >> num;
    return num;
}

void tellStory(const std:: string *const name, const std:: string *const noun,
               const std:: string *const bodyPart, const std:: string *const verb,
               const int *const number)
{
    std:: cout << "\nHere's your story:\n";
    std:: cout << "The famous explorer ";
    std:: cout << *name;
    std:: cout << " had nearly given up a life long quest to find\n";
    std:: cout << "The Lost City of ";
    std:: cout << *noun;
    std:: cout << " when one day, the ";
    std:: cout << *noun;
    std:: cout << " found the explorer.\n";
    std:: cout << "Surrounded by ";
    std:: cout << *number;
    std:: cout << " " << *noun;
    std:: cout << ", a tear came to ";
    std:: cout << *name << "'s ";
    std:: cout << *bodyPart << ".\n";
    std:: cout << "After all this time, the quest was finally over. ";
    std:: cout << "And then, the ";
    std:: cout << *noun << "\n";
    std:: cout << "promptly devoured ";
    std:: cout << *name << ". ";
    std:: cout << "The moral of the story? Be careful what you ";
    std:: cout << *verb;
    std:: cout << " for.";
}



void madLib()
{

    
    std:: cout << "\tMad Lib v3.0\n\n";
    
    std:: cout << "Answer the following questions to help create a new story.\n\n";
    
    std:: string name = askText("Please enter a name: ");
    std:: string noun = askText("Please enter a plural noun: ");
    int number = askNumber("Please enter a number: ");
    std:: string bodyPart = askText("Please enter a body part: ");
    std:: string verb = askText("Please enter a verb: ");
    
    std:: string *pName = &name;
    std:: string *pNoun = &noun;
    std:: string *pBodyPart = &bodyPart;
    std:: string *pVerb = &verb;
    int *pNumber = &number;
    
    tellStory(pName, pNoun, pBodyPart, pVerb, pNumber);
    std:: cin.ignore();
    std:: cin.get();
}

