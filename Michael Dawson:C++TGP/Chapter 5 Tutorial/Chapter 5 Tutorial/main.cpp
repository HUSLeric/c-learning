//
//  main.cpp
//  Ch 5 Tutorial
//
//  Created by H U $ L on 12/22/13.
//  Copyright (c) 2013 Eric Wallace. All rights reserved.
//

#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <string>
#include <cctype>


/* Rewrite the Hangman game from Chapter 4 using functions. Include a function to get the player’s guess and another function to determine whether the player’s guess is in the secret word.*/

void clearscreen();
void whatswrong();
void hangman();
char getGuess(std:: string &used);
void checkGuess(char guess, std:: string secretword, std:: string &sofar,
                int &wrong);
void exercise3();

int main()
{
    
    enum GAMESTATE {MAIN, EX1, EX2, EX3, OFF, NUM_OF_STATES};
    
    int gamestate = MAIN;
    while(gamestate != OFF)
    {
        clearscreen();
        std:: cout << "Chapter 5 Exercises\n\n";
        
        std:: cout << "1 - Whats wrong with the code?\n";
        std:: cout << "2 - Hangman\n";
        std:: cout << "3 - Functions\n";
        std:: cout << "4 - Quit Program\n\n";
        
        int choice;
        std:: cout << "Your choice: ";
        std:: cin >> choice;
        
        switch(choice)
        {
            case 1:
                clearscreen();
                gamestate = EX1;
                whatswrong();
                break;
            case 2:
                clearscreen();
                gamestate = EX2;
                hangman();
                break;
            case 3:
                clearscreen();
                gamestate = EX3;
                exercise3();
                break;
            case 4:
                std:: cout << "Thank you for using my program." << std:: endl;
                gamestate = OFF;
                break;
            default:
                gamestate = MAIN;
                clearscreen();
                std:: cout << "You entered invalid option number." << std:: endl;
                std:: cin.ignore();
                std:: cin.get();
                break;
        }
    }
    
}

void whatswrong()
{
    std:: cout << "Whats wrong with the following prototype ?\n\n";
    
    std:: cout << "int askNumber(int low = 1, int high);\n\n";
    
    std:: cout << "By using a default argument, all the arguments behind it\n";
    std:: cout << "when passing in the arguments would have to be left blank\n";
    std:: cout << "so high would never get initialized.\n";
    
    std:: cin.ignore();
    std:: cin.get();
}

char getGuess(std:: string &used)
{
    char guess;
    
    std:: cout << "\nYour guess: ";
    std:: cin >> guess;
    guess = tolower(guess);
    
    while(used.find(guess) != std:: string:: npos)
    {
        std:: cout << "\nYou've already guessed " << guess << std:: endl;
        std:: cout << "Enter your guess: ";
        std:: cin >> guess;
        guess = toupper(guess);
    }
    
    used += guess;
    
    return guess;
}

void checkGuess(char guess, std:: string secretword, std:: string &sofar,
                int &wrong)
{
    if(secretword.find(guess) != std:: string:: npos)
    {
        std:: cout << "That's right ! " << guess << " is in the word.\n";
        
        // update soFar to get the new guessed letter
        for(int i = 0; i < secretword.length(); ++i)
        {
            if(secretword[i] == guess)
            {
                sofar[i] = guess;
            }
        }
    }
    else
    {
        std:: cout << "Sorry, " << guess << " isn't in the word.\n";
        ++wrong;
    }
}

void hangman()
{
    // Max number of incorrect guesses allowed
    const int MAX_WRONG = 8;
    // seed random num generator
    srand(static_cast<unsigned int>(time((0))));
    // create list of words to guess
    std:: vector<std:: string> words;
    words.push_back("basketball");
    words.push_back("battlefield");
    words.push_back("women");
    words.push_back("money");
    words.push_back("hindu");
    // shuffle list
    random_shuffle(words.begin(), words.end());
    // pick word to guess for
    const std:: string secretWord = words[0];
    // number of incorrect guesses
    int wrong = 0;
    // word guessed so far
    std:: string soFar(secretWord.size(), '-');
    // letters already guessed
    std:: string used = "";
    // letter guessed by user
    char guess;
    
    
    std:: cout << "Welcome to Hangman. Good Luck ! \n";
    while((wrong < MAX_WRONG) && (soFar != secretWord))
    {
        std:: cout << "\n\nYou have " << (MAX_WRONG - wrong);
        std:: cout << "incorrect guesses left.\n";
        std:: cout << "\nYou've used the following letters:\n" << used;
        std:: cout << "\n\nSofar, the word is:\n" << soFar << std:: endl;
        
        // Gets guess
        guess = getGuess(used);
        
        // checks guess
        checkGuess(guess, secretWord, soFar, wrong);
    }
    
    if (wrong == MAX_WRONG)
    {
        std:: cout << "You have been hanged ! X_X" << std:: endl;
    }
    else
    {
        std:: cout << "YOU GUESSED THE WORD !\n";
        std:: cout << "The word was '" << secretWord << "' ! \n";
    }
    
    std:: cout << "\nThank you for playing Hangman ! " << std:: endl;
    std:: cout << "Press Enter to continue." << std:: endl;
    std:: cin.ignore();
    std:: cin.get();
    
}

void exercise3()
{
    std:: cout << "Didn't understand question , so didn't do it . lololol\n";
    std:: cin.ignore();
    std:: cin.get();
}
void clearscreen()
{
    for (int i = 0; i < 50; ++i)
    {
        std:: cout << "\n\n";
    }
}

