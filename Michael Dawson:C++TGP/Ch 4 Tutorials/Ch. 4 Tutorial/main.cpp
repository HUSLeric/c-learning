/*
 Eric Wallace
 December 21, 2013
 Chapter 4 Exercises
 Beginning C++ Through Game Programming, Third Edition
 */

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>

void clearscreen();
void listem();
void whatswrong();
void pseudocode();

int main()
{
    int exercise;
    while(exercise != 4)
    {
        clearscreen();
        std:: cout << "Chapter 4 Exercises\n";
    
        std:: cout << "\n1 - LISTEM\n";
        std:: cout << "2 - Whats wrong with the code ?\n";
        std:: cout << "3 - Pseudocode for World Jumble\n";
        std:: cout << "4 - Exit Program\n";
    
        
        std:: cout << "\nWhich would you like to choose ?\n";
        std:: cout << "Exercise: ";
        std:: cin >> exercise;
        
        switch (exercise)
        {
            case 1:
                clearscreen();
                listem();
                break;
            case 2:
                clearscreen();
                whatswrong();
                break;
            case 3:
                clearscreen();
                pseudocode();
                break;
            case 4:
                break;
            default:
                std:: cout << "You did not enter a valid number.\n";
        }
    }
    
}

void listem()
{
    std:: cin.clear();
    std:: cin.ignore();
    
    std:: vector<std:: string> games;
    std:: vector<std:: string>:: iterator myIterator;
    std:: vector<std:: string>:: const_iterator iter;
    
    // Header of Program !
    std:: cout << "\tLISTEM v1.0\n";
    std:: cout << "Make a list of anything you want !\n";
    
    // Gets name of list
    std:: string listname;
    std:: cout << "\nWhat would you like to name your list ?\n";
    getline(std:: cin, listname);
    
    int function;
    do {
        clearscreen();
        std:: cout << "\tLISTEM v1.0\n";
        
        // Game Functions
        std:: cout << "\n1 - Add something to list\n";
        std:: cout << "2 - Remove something from list\n";
        std:: cout << "3 - Save List and Exit\n";
        
        // Display list
        std:: cout << "\n---------- " << listname << " ----------\n";
        
        for(iter = games.begin(); iter != games.end(); ++iter)
        {
            std:: cout << *iter << std:: endl;
        }
        
        // User Input on what to do
        std:: cout << "What To Do: ";
        std:: cin >> function;
        
        if(function == 1)
        {
            std:: cin.clear();
            std:: cin.ignore();
            clearscreen();
            std:: string addition;
            std:: cout << "What would you like to add ?\n";
            std:: cout << "Add: ";
            getline(std:: cin, addition);
            
            games.push_back(addition);
        }
        else if(function == 2)
        {
            std:: cin.clear();
            std:: cin.ignore();
            clearscreen();
            std:: string remove;
            std:: cout << "What would you like to remove ?\n\n";
            
            for(iter = games.begin(); iter != games.end(); ++iter)
            {
                std:: cout << *iter << std :: endl;
            }
            
            std:: cout << "\nRemove: ";
            getline(std:: cin, remove);
            
            myIterator = find(games.begin(), games.end(), remove);
            games.erase(myIterator);
            
            if (myIterator != games.end())
            {
                clearscreen();
                std:: cout << "POOF !" << std:: endl;
                std:: cin.ignore();
                std:: cin.get();
                
            }
            else
            {
                std:: cout << "'" << remove << "' is not in your list.\n";
                std:: cin.ignore();
                std:: cin.get();
            }
            
        }
        else if(function < 1 || function > 3)
        {
            clearscreen();
            std:: cout << "You entered wrong function. Press Enter";
            std:: cin.ignore();
            std:: cin.get();
        }
        
    } while (function != 3);
    
    clearscreen();
    std:: cout << "The list '" << listname << "' is saved. :)" << std:: endl;
    std:: cin.ignore();
    std:: cin.get();
    
    clearscreen();
    // Display list
    std:: cout << "\n---------- " << listname << " ----------\n";
    
    for(iter = games.begin(); iter != games.end(); ++iter)
    {
        std:: cout << *iter << std:: endl;
    }
    
    std:: cout << "\nThank you for using Listem." << std:: endl;
    
    
    
}

void whatswrong()
{
    std:: cout << "Whats wrong with this code ? " << std:: endl;
    
    std:: cout << "Assuming that scores is a vector that holds elements of\n";
    std:: cout << "type int." << std:: endl;
    
    std:: cout << "\nvector<int>::iterator iter;\n";
    std:: cout << "//increment each score\n";
    std:: cout << "for(iter = scores.begin(); iter != scores.end(); ++iter)\n";
    std:: cout << "{\n";
    std:: cout << "     iter++;\n";
    std:: cout << "}\n";
    
    std:: cout << "\n The iterator iter is not referenced when executing the\n";
    std:: cout << "code. So basically its just moving through the vector scores\n";
    std:: cout << "twice as fast because it increments twice instead of increm-\n";
    std:: cout << "enting the score by one." << std:: endl;
    
    std:: cin.ignore();
    std:: cin.get();
    
}

void pseudocode()
{
    std:: cout << "PSUEDOCODE FOR WORD JUMBLE" << std:: endl;
    
    std:: cout << "\nCreate array of words to use.\n";
    std:: cout << "Create parallel array of scores for each word.\n";
    std:: cout << "Randomly choose one of those words.\n";
    std:: cout << "Display prompt to game\n";
    std:: cout << "While user guess is not the same as word picked, pick again\n";
    std:: cout << "       Ask if they want hint.\n";
    std:: cout << "          If yes, display hint and deduct points from wScore\n";
    std:: cout << "If user guess is the same, congratulate them and show word.\n";
    std:: cout << "Ask if they wanna play again\n";
    std:: cout << "If yes, then rerun. Else go back to main menu.\n";
    
    std:: cin.ignore();
    std:: cin.get();
    
    
    
}

void clearscreen()
{
    for(int i = 0; i < 50; i++)
    {
        std:: cout << "\n" << std:: endl;
    }
}