/*
 Eric Wallace
 HUSL
 December 22, 2013
 */

#include <iostream>

void clearscreen(); // clears output screen
void madLib(); // mad lib main menu
void heroInfo(const std:: string &name, const std:: string &land,
              const std:: string &weapon, const int &years);
void displayStory(const std:: string &name, const std:: string &land,
                  const std:: string &weapon, const int &years);
void exercise2();   // exercise 2
void exercise3();   // exercise 3
void errorScreen(); // displays error to user
void exitMessage(); // displays exit message

int main()
{

    bool playing = true;
    
    while(playing)
    {
        clearscreen();
        int exercise;
        std:: cout << "Chapter 6 Exercises\n\n";
        
        std:: cout << "1 - Mad Lib v2.0\n";
        std:: cout << "2 - Whats wrong with this program ?\n";
        std:: cout << "3 - Whats wrong with this code ?\n";
        std:: cout << "4 - Exit program\n\n";
        
        std:: cout << "Which exercise do you want to view:";
        std:: cin >> exercise;
        
        switch(exercise)
        {
            case 1:
                clearscreen();
                madLib();
                break;
            case 2:
                clearscreen();
                exercise2();
                std:: cin.ignore();
                std:: cin.get();
                break;
            case 3:
                clearscreen();
                exercise3();
                std:: cin.ignore();
                std:: cin.get();
                
                break;
            case 4:
                clearscreen();
                exitMessage();
                playing = false;
                break;
            default:
                clearscreen();
                errorScreen();
                std:: cin.ignore();
                std:: cin.get();
                
                break;
                
        }

    }
    
    
}

void clearscreen()
{
    for(unsigned int i = 0; i < 50; ++i)
    {
        std:: cout << "\n\n";
    }
}

void errorScreen()
{
    std:: cout << "Wrong input !" << std:: endl;
    
}


void exitMessage()
{
    std:: cout << "Thank you for using my program. - Eric";
}

void exercise2()
{
    std:: cout << "Whats wrong with the following program?" << std:: endl;
    
    std:: cout << "\nint main()\n";
    std:: cout << "{\n";
    std:: cout << "      int score;\n";
    std:: cout << "      score = 1000;\n";
    std:: cout << "      float& rScore = score;\n";
    std:: cout << "      return 0;\n";
    std:: cout << "}\n";
    
    std:: cout << "\n\nYou cant reference a variable of different types.\n";
    std:: cout << "The variable types need to both be float or int not both.";
    
}

void exercise3()
{
    std:: cout << "Whats wrong with the following function?" << std:: endl;
    
    std:: cout << "\nint& plusThree(int number)\n";
    std:: cout << "{\n";
    std:: cout << "       int threeMore = number +3;\n";
    std:: cout << "       return threeMore;\n";
    std:: cout << "}\n";
    
    std:: cout <<"\n\nYour returning an int and not returning a reference.";
}

void heroInfo(std:: string &name, std:: string &land,
              std:: string &weapon, int &years)
{
    std:: cout << "Please Enter in the following information !\n\n";
    
    std:: cout << "Your name: ";
    std:: cin >> name;
    std:: cout << "\nWhere are you from: ";
    std:: cin >> land;
    std:: cout << "\nFavorite number: ";
    std:: cin >> years;
    std:: cout << "\nFavorite Weapon: ";
    std:: cin >> weapon;
    
}

void displayStory(const std:: string &name, const std:: string & land,
                  const std:: string &weapon, const int &years)
{
    std:: cout << "In the land far far away called " << land;
    std:: cout << " was a young hero named " << name << ".";
    std:: cout << "\n" << name << " didn't know it yet, but their adventure";
    std:: cout << " was about to unfold !";
    
    std:: cout << "\n\n\n" << years << " Years pass by . . .\n\n\n";
    
    std:: cout << name << " got killed by a " << weapon << ".";
    std:: cout << name << " is now dead. x_x\n\n";
    
    std:: cout << "The end." << std:: endl;
}

void madLib()
{
    std:: cout << "\tMad Lib v2.0\n";
    std:: cout << "Where with the help of you, we create a story !\n\n";
    std:: string name, land, weapon;
    int years;
    
    heroInfo(name, land, weapon, years);
    
    displayStory(name, land, weapon, years);
    
    std:: cin.ignore();
    std:: cin.get();
}


