/*
 Eric Wallace
 Saturday, January 4th, 2014
 Chapter 8 Tutorial
 Classes / Structs
 */


#include <iostream>

void clearscreen();
void errorScreen();
void exitMessage();
void ct1();
void whatsWrong();

class Critter {
private:
    int mhunger; // critters hunger levels
    int mboredom; // critters boredom level
    void PassTime(int time = 1); // simulates time passing increases hunger/boredom
    int GetMood() const; // calculates the mood of the critter
    
public:
    Critter(int hunger = 0, int boredom = 0); // constructor
    void Talk(); // critter talks back
    void Eat(int food = 4); // feeding critter function
    void Play(int fun = 4); // playing with critter function
    void Show(); // shows critter levels
    
    
};

Critter:: Critter(int hunger, int boredom):
mhunger(hunger),
mboredom(boredom)
{}

inline int Critter:: GetMood() const
{
    return (mhunger + mboredom);
}

void Critter:: PassTime(int time)
{
    mhunger += time;
    mboredom += time;
}

void Critter:: Talk()
{
    std:: cout << "I'm a critter and I feel ";
    
    int mood = GetMood();
    
    // Tells Mood
    if(mood > 15)
    {
        std:: cout << "mad.\n";
    }
    else if(mood > 10)
    {
        std:: cout << "frustrated.\n";
    }
    else if(mood > 5)
    {
        std:: cout << "okay.\n";
    }
    else
    {
        std:: cout << "happy.\n";
    }
    
    // Hints How Hungry
    if(mhunger > 15)
    {
        std:: cout << "I'm STARVING !\n";
    }
    else if(mhunger > 10)
    {
        std:: cout << "I'm really hungry :(\n";
    }
    else if(mhunger > 5)
    {
        std:: cout << "I'm getting hungry ... \n";
    }
    else
    {
        std:: cout << "I'm full ^_^.\n";
    }
    
    // Hints How Bored
    
    if(mboredom > 15)
    {
        std:: cout << "I'm DYING OF BOREDOM !\n";
    }
    else if(mboredom > 10)
    {
        std:: cout << "I'm really bored, play with me please :(\n";
    }
    else if(mboredom > 5)
    {
        std:: cout << "I'm getting bored ... \n";
    }
    else
    {
        std:: cout << "I'm tired z_z.\n";
    }
    
    PassTime();
    
}

void Critter:: Eat(int food)
{
    std:: cout << "****BURPS****\n";
    mhunger -= food;
    
    if(mhunger < 0)
    {
        mhunger = 0;
    }
    
    PassTime();
}

void Critter:: Play(int fun)
{
    std:: cout << "Wheeeeeee!\n";
    
    mboredom -= fun;
    
    if(mboredom < 0)
    {
        mboredom = 0;
    }
    
    PassTime();
}

void Critter:: Show()
{
    std:: cout << "Hunger: " << mhunger << std:: endl;
    std:: cout << "Boredom: " << mboredom << std:: endl;
    
    
    if(mhunger > mboredom)
    {
        if(mhunger > 5)
        {
            std:: cout << "\nYou should feed your critter." << std:: endl;
        }
        else
        {
            std:: cout << "\nYour critter is full. Play with it." << std:: endl;
        }
    }
    else if(mboredom > mhunger)
    {
        if(mboredom > 5)
        {
            std:: cout << "\nYou should play with your critter." << std:: endl;
        }
        else
        {
            std:: cout << "\nYour critter is tired. Feed it." << std:: endl;
        }
    }

}
int main()
{

    bool playing = true;
    
    while(playing)
    {
        clearscreen();
        int exercise;
        std:: cout << "Chapter 8 Exercises\n\n";
        
        std:: cout << "1 - Critter Caretaker v1.0\n";
        std:: cout << "2 - Whats wrong with the code?\n";
        std:: cout << "3 - Exit program\n\n";
        
        std:: cout << "Which exercise do you want to view:";
        std:: cin >> exercise;
        
        switch(exercise)
        {
            case 1:
                clearscreen();
                ct1();
                break;
            case 2:
                clearscreen();
                whatsWrong();
                std:: cin.ignore();
                std:: cin.get();
                break;
            case 3:
                clearscreen();
                exitMessage();
                playing = false;
                break;
            default:
                clearscreen();
                errorScreen();
                std:: cin.ignore();
                std:: cin.get();
                
                break;
                
        }
        
    }
    
}

void ct1()
{
    Critter crit;
    
    
    bool kplay = true;
    
    
    
    int choice;
    
    while(kplay == true)
    {
        clearscreen();
        std:: cout << "\tCritter Caretaker v1.0\n\n";
        
        std:: cout << "0 - Quit\n";
        std:: cout << "1 - Listen To Your Critter\n";
        std:: cout << "2 - Feed Your Critter\n";
        std:: cout << "3 - Play With Your Critter\n";
        std:: cout << "4 - Show Critter Levels\n\n";
        
        std:: cout << "Choice: ";
        std:: cin >> choice;
        
        switch (choice)
        {
            case 0:
                clearscreen();
                // Exits Program so does nothing.
                kplay = false;
                break;
            case 1:
                clearscreen();
                crit.Talk();
                std:: cin.ignore();
                std:: cin.get();
                break;
            case 2:
                clearscreen();
                crit.Eat();
                std:: cin.ignore();
                std:: cin.get();
                break;
            case 3:
                clearscreen();
                crit.Play();
                std:: cin.ignore();
                std:: cin.get();
                break;
            case 4:
                clearscreen();
                crit.Show();
                std:: cin.ignore();
                std:: cin.get();
                break;
            default:
                clearscreen();
                errorScreen();
                std:: cin.ignore();
                std:: cin.get();
                break;
        }
        
    }
        
    
}

void whatsWrong()
{
    std:: cout << "What design problem does the following program have ?\n\n";
    
    std:: cout << "#include <iostream>\n";
    std:: cout << "using namespace std;\n\n";
    
    std:: cout << "class Critter\n";
    std:: cout << "{\n";
    std:: cout << "public:\n";
    std:: cout << "     int GetHunger() const {return m_Hunger;}\n";
    std:: cout << "private:\n";
    std:: cout << "     int m_Hunger;\n";
    std:: cout << "};\n\n";
    
    std:: cout << "int main()\n";
    std:: cout << "{\n";
    std:: cout << "     Critter crit;\n";
    std:: cout << "     cout << crit.GetHunger() << endl;\n";
    std:: cout << "     return 0;\n";
    std:: cout << "}\n";
    
    
    std:: cout << "\nThe semicolon is inside the curly brackets instead of\n";
    std:: cout << "being on the outside, after 'return m_Hunger'.";
    
}

void clearscreen()
{
    for(int i = 0; i < 50; ++i)
    {
        std:: cout << "\n\n";
    }
}

void exitMessage()
{
    std:: cout << "Thank you for using my program ! Good Bye !" << std:: endl;
}

void errorScreen()
{
    std:: cout << "Wrong Input !" << std:: endl;
}
